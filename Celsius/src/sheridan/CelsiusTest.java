package sheridan;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class CelsiusTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testconvertFromFahrenheitPositive() {
		int expected = Celsius.convertFromFahrenheit(25);
		int actual = Celsius.convertFromFahrenheit(25);

		assertEquals(expected, actual);
	}

	@Test
	public void testconvertFromFahrenheitNegative() {
		int expected = Celsius.convertFromFahrenheit(20);
		int actual = Celsius.convertFromFahrenheit(25);
		assertNotEquals(expected, actual);
	}

	/*
	 * @Test(expected = NumberFormatException.class) public void
	 * testconvertFromFahrenheitException() { int actual =
	 * Celsius.convertFromFahrenheit(7); assertFalse(2.0 == actual); }
	 */
	 
	@Test
	public void testconvertFromFahrenheitBoundaryIn() {

		int expected = Celsius.convertFromFahrenheit(-14);
		int actual = Celsius.convertFromFahrenheit(6);
		assertFalse(expected >= actual);
		;
	}

	@Test
	public void testconvertFromFahrenheitBoundaryOut() {
		int expected = Celsius.convertFromFahrenheit(-14);
		int actual = Celsius.convertFromFahrenheit(7);
		assertFalse(expected > actual);
	}

}
